# SimWar

## Introduction

This scholar project want to resolve a given specification.
The purpose is to instanciate robot and make them fight.

## Resources

The UML diagrams of this project is in `/Resources/UML` folder in .vpp format (Visual Paradigm) and also in format `.pnj`.

A more complete report, written in French, is in `/Ressources/Report`.

## Running project

This project was developed with IntelliJ with the OpenJDK 17.0.2 compiler.

You just need to run the Main.java with an IDE (like Eclipse or IntelliJ) or compile the project to an executable.

## Features

You can customize each robot in main by changing parameters.

The first parameter is the name.

The 2nd, 3rd, 4th, 5th, 6th, 7th parameters are the statistic one (attack, defense, speed, dodge, accuracy & maxHealth). Be careful, the sum of these parameters canot be superior to 20.

The 8th, 9th, 10th, 11th, 12th parameters are the gear index ones (lWeapon, rWeapon, head, chest & bottom). According to GearFactory, you can set these parameters by 0, 1 or 2.

The last (13th) parameter is the special skill one. You can choose between `Escape`, `Repair` or `Hack`.
