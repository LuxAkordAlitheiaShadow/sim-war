package com.lux.simwar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

final public class IHM
{
    // Attributes.
    private static IHM ihmInstance = null;

    // Methods.

    // Getting class instance


    public static IHM getIhmInstance()
    {
        // Singleton not instanced statement
        if ( ihmInstance == null )
        {
            ihmInstance = new IHM();
        }
        return ihmInstance;
    }

    // Printing message on user console
    public void printConsole(String message)
    {
        System.out.println(message);
    }

    // Getting message from user console
    public String promptConsole()
            throws IOException
    {
        // Allowing user to write in console
        BufferedReader consoleInput = new BufferedReader(
                new InputStreamReader(System.in)
        );
        // Returning console input message
        return consoleInput.readLine();
    }
}
