package com.lux.simwar;


import com.lux.simwar.gear.GearFactory;
import com.lux.simwar.gear.Head;
import com.lux.simwar.robot.Robot;
import com.lux.simwar.robot.Stat;


public class Main
{
    public static void main(String[] args) {
        // Initializing classes
        SimWar simWar = new SimWar();

        Robot robot1 = new Robot("T1000", 4, 3, 1, 1, 1, 10, 0,1,0, 0, 0, "Hack");
        Robot robot2 = new Robot("Wall-E", 2, 5, 1, 1, 1, 10, 1, 2,1, 2, 1, "Repair");

        simWar.startFight(robot1,robot2);
    }
}
