package com.lux.simwar;

import com.lux.simwar.robot.Robot;

import java.util.ArrayList;
import java.util.Random;

final public class SimWar
{
    // Attributes.
    private final ArrayList<Robot> robots = new ArrayList<Robot>();
    private int currentTurn;
    private final int areaSize;
    private final IHM ihm = IHM.getIhmInstance();

    // Methods.

    // Constructor.
    public SimWar()
    {
        this.areaSize = 100;
    }

    // Getters.
    private int getCurrentTurn()
    {
        return this.currentTurn;
    }

    public int getAreaSize()
    {
        return this.areaSize;
    }

    // Setters.
    private void setCurrentTurn(int newCurrentTurn)
    {
        this.currentTurn = newCurrentTurn;
    }

    // Initialisation of robots
    public void addingRobots(Robot robot1, Robot robot2) {
        this.robots.add(robot1);
        ihm.printConsole(robots.get(0).getStat().toString());
        this.robots.add(robot2);
        this.ihm.printConsole(robots.get(1).getStat().toString());
    }

    public void startFight(Robot robot1, Robot robot2)
    {
        addingRobots(robot1, robot2);
        while(robots.get(0).isAlive() && robots.get(1).isAlive())
        {
            doCycle();
        }

        // First robot win statement
        if ( this.robots.get(0).isAlive() )
        {
            this.ihm.printConsole("Robot " + this.robots.get(0).getName() + " win !");
        }
        // Second robot win statement
        else
        {
            this.ihm.printConsole("Robot " + this.robots.get(1).getName() + " win !");
        }
    }

    // Cycle execution
    public void doCycle()
    {
        this.ihm.printConsole( "\nCycle number : " + this.getCurrentTurn() + ".\n");
        this.setCurrentTurn(getCurrentTurn() + 1);

        // Getting speediest robot
        int robotId = this.getSpeediestRobot();
        int otherRobotId = (robotId + 1) % 2;

        // Speediest robot turn
        this.ihm.printConsole(robots.get(robotId).toString());
        this.ihm.printConsole(robots.get(otherRobotId).toString());
        this.ihm.printConsole( "Robot " + robots.get(robotId).getName() + " begin.");
        // Speediest robot isn't under hack statement
        if ( robots.get(robotId).getStunRemain() == 0 )
        {
            robots.get(robotId).doTurn(robots.get(otherRobotId), this);
        }
        // Speediest robot is under hack statement
        else
        {
            this.ihm.printConsole("But he's under hack !");
            // Decrementing stun remaining
            robots.get(robotId).setStunRemain( robots.get(robotId).getStunRemain() - 1 );
        }
        this.ihm.printConsole("");
        // Other robot still alive statement
        if ( robots.get(otherRobotId).isAlive() )
        {
            // Other robot turn
            this.ihm.printConsole(robots.get(robotId).toString());
            this.ihm.printConsole(robots.get(otherRobotId).toString());
            this.ihm.printConsole("Robot " + robots.get(otherRobotId).getName() + " begin.");
            // Other robot isn't under hack statement
            if ( robots.get(otherRobotId).getStunRemain() == 0 )
            {
                robots.get(otherRobotId).doTurn(robots.get(robotId), this);
            }
            // Other robot is under hack statement
            else
            {
                this.ihm.printConsole("But he's under hack !");
                // Decrementing stun remaining
                robots.get(otherRobotId).setStunRemain( robots.get(otherRobotId).getStunRemain() - 1 );
            }
            this.ihm.printConsole("");
        }
    }

    // Getting speediest robot
    private int getSpeediestRobot()
    {
        // Robot 1 speedier than robot 2 statement
        if ( robots.get(0).getStat().getSpeed() > robots.get(1).getStat().getSpeed() )
        {
            return 0;
        }
        // Robot 2 speedier than robot 2 statement
        else if ( robots.get(0).getStat().getSpeed() < robots.get(1).getStat().getSpeed() )
        {
            return 1;
        }
        // Robot 1 is as fast as robot 2 statement
        else
        {
            Random randomizer = new Random();
            return randomizer.nextInt(2);
        }
    }
}
