package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

public final class Bottom extends Gear
{
    //Attributes
    private String type;

    //Creator
    public Bottom(String name, Stat modif)
    {
        super(modif);
        this.name=name;
        this.type="Bottom";
    }
    //Getter
    public String getType() {
        return type;
    }
}
