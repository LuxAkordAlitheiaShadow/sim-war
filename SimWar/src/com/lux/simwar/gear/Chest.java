package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

public final class Chest extends Gear
{
    //Attributes
    private String type;

    //Creator
    public Chest(String name, Stat modif)
    {
        super(modif);
        this.name=name;
        this.type="Chest";
    }
    //Getter
    public String getType() {
        return type;
    }
}
