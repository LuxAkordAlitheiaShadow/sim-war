package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

public abstract class Gear
{
    // Attributes
    protected String name;
    protected Stat modificator;

    //Constructor
    Gear(Stat modif){
        //We check if the statistic follows the rule
        modif.verifyStats();
        modificator=modif;
    }
    final public Stat getStat()
    {
        return this.modificator;
    }
    final public String getName(){return this.name;}
}
