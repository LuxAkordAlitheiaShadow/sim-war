package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

import java.util.ArrayList;
import java.util.List;

public final class GearFactory
{
    private final List<Head> listHeadGear = new ArrayList<Head>();
    private final List<Chest> listChestGear = new ArrayList<Chest>();
    private final List<Bottom> listBottomGear = new ArrayList<Bottom>();
    private final List<Weapon> listWeaponGear = new ArrayList<Weapon>();

    //Creator
    public GearFactory()
    {
        // We create all available gears
        Head head1 = new Head("Iron Helmet",new Stat(1,2,2,2,5,8));
        Head head2 = new Head("Steel cage",new Stat(2,1,2,4,4,7));
        Head head3 = new Head("Rubber visor",new Stat(3,1,3,2,3,6));

        listHeadGear.add(head1);
        listHeadGear.add(head2);
        listHeadGear.add(head3);

        Chest chest1 = new Chest("Iron chestplate",new Stat(1,2,2,2,5,8));
        Chest chest2 = new Chest("Reinforced plastic chest",new Stat(2,1,2,4,4,7));
        Chest chest3 = new Chest("Spiked armor",new Stat(3,1,3,2,3,6));

        listChestGear.add(chest1);
        listChestGear.add(chest2);
        listChestGear.add(chest3);

        Bottom bottom1 = new Bottom("Iron Boots",new Stat(1,2,2,2,5,8));
        Bottom bottom2 = new Bottom("Plastic wheel",new Stat(2,1,2,4,4,7));
        Bottom bottom3 = new Bottom("Spider legs",new Stat(3,1,3,2,3,6));

        listBottomGear.add(bottom1);
        listBottomGear.add(bottom2);
        listBottomGear.add(bottom3);

        Weapon weapon1 = new Weapon("Rifle",new Stat(3,0,0,0,6,0),10);
        Weapon weapon2 = new Weapon("Spear",new Stat(5,0,0,0,5,0),6);
        Weapon weapon3 = new Weapon("Hammer",new Stat(8,0,0,0,7,0),3);

        listWeaponGear.add(weapon1);
        listWeaponGear.add(weapon2);
        listWeaponGear.add(weapon3);

    }

    // Getter
    public List<Head> getListHeadGear() {
        return listHeadGear;
    }

    public List<Chest> getListChestGear() {
        return listChestGear;
    }

    public List<Bottom> getListBottomGear() {
        return listBottomGear;
    }

    public List<Weapon> getListWeaponGear() {
        return listWeaponGear;
    }
}
