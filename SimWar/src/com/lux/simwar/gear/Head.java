package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

public final class Head extends Gear
{
    //Attributes
    private String type;

    //Creator
    public Head(String name, Stat modif)
    {
        super(modif);
        this.name=name;
        this.type="Head";
    }

    //Getter
    public String getType() {
        return type;
    }
}
