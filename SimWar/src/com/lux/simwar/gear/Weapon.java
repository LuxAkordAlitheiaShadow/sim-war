package com.lux.simwar.gear;

import com.lux.simwar.robot.Stat;

public final class Weapon extends  Gear
{
    // Attributes
    private String type;
    //Range of this weapon
    private int range;
    //Boolean of if a weapon is used during a action
    private boolean isUsed;

    // Constructor.
    public Weapon(String name, Stat modif, int range)
    {
        super(modif);
        this.name=name;
        this.type="Weapon";
        this.range=range;
        this.isUsed = false;
    }

    // Getters.
    public int getRange() {
        return this.range;
    }
    public String getType() {return type;}
    public boolean getIsntUsed(){return !this.isUsed;}

    // Setters.
    private void setIsUsed(boolean isUsed){this.isUsed=isUsed;}

    // Methods.

    //When a weapon is used, isUsed is set to true
    public void useWeapon()
    {
        setIsUsed(true);
    }

    // Reset the value of isUsed to false
    public void resetWeapon(){
        setIsUsed(false);
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "type='" + type + '\'' +
                ", range=" + range +
                ", isUsed=" + isUsed +
                '}';
    }
}
