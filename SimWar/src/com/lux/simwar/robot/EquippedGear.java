package com.lux.simwar.robot;

import com.lux.simwar.gear.Bottom;
import com.lux.simwar.gear.Chest;
import com.lux.simwar.gear.Head;
import com.lux.simwar.gear.Weapon;

final public class EquippedGear {
    //Attributes
    Weapon leftArm;
    Weapon rightArm;
    Bottom bottom;
    Chest chest;
    Head head;

    //Constructor
    public EquippedGear(){

    }
}
