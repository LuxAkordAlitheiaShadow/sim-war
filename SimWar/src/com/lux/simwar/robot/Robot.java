package com.lux.simwar.robot;
import com.lux.simwar.IHM;
import com.lux.simwar.SimWar;
import com.lux.simwar.gear.GearFactory;
import com.lux.simwar.gear.Weapon;

final public class Robot {

    //Attributes
    // Name of the robot
    private final String name;
    // Robot's gears
    private final EquippedGear gear;
    private final Skill skill;
    private Stat stat;
    // Distance between the two robots
    static private int enemyDistance;
    // Timmer if the robot is stuned
    private int stunRemain = 0;
    // Timmer for use the special capacity
    private int specialCooldownRemain = 0;
    // The smallest distance between the weapons of the robot
    private int minRange;

    //Constructor
    public Robot(String name, int attack, int defense, int speed, int dodge, int accuracy, int maxHealth, int lWeapon, int rWeapon, int head, int chest, int bottom, String special) {
        this.name = name;
        this.gear = new EquippedGear();
        this.skill = new Skill(special);
        enemyDistance = 20;
        Stat basicsStats = verifBasicsStats(attack,defense,speed,dodge,accuracy,maxHealth);
        initGear(lWeapon, rWeapon, head, chest, bottom);
        initStat(basicsStats);
        initMinRange();
    }

    //Methods.

    // Getters.
    public Stat getStat()
    {
        return this.stat;
    }
    public int getEnemyDistance()
    {
        return enemyDistance;
    }
    public int getStunRemain()
    {
        return this.stunRemain;
    }
    public String getName()
    {
        return this.name;
    }
    public EquippedGear getGear()
    {
        return this.gear;
    }
    public int getMinRange()
    {
        return this.minRange;
    }
    public int getSpecialCooldownRemain(){return specialCooldownRemain;}

    // Setters.
    public void setEnemyDistance(int newEnemyDistance)
    {
        enemyDistance = newEnemyDistance;
    }
    public void setStunRemain(int newStunRemain)
    {
        this.stunRemain = newStunRemain;
    }
    public void setSpecialCooldownRemain(int specialCooldown)
    {
        this.specialCooldownRemain = specialCooldown;
    }


    //This function equips selected gears to the robot
    private void initGear(int lWeapon, int rWeapon, int head, int chest, int bottom)
    {
        GearFactory gearFactory = new GearFactory();
        gear.leftArm = gearFactory.getListWeaponGear().get(lWeapon);
        gear.rightArm = gearFactory.getListWeaponGear().get(rWeapon);
        gear.head = gearFactory.getListHeadGear().get(head);
        gear.chest = gearFactory.getListChestGear().get(chest);
        gear.bottom = gearFactory.getListBottomGear().get(bottom);
    }

    //This function init robot stats with initial stats and all gears stats
    private void initStat(Stat basicStats)
    {
        int modifAttack = gear.head.getStat().getAttack() + gear.chest.getStat().getAttack() +gear.bottom.getStat().getAttack();
        int modifDefense = gear.head.getStat().getDefense() + gear.chest.getStat().getDefense() +gear.bottom.getStat().getDefense();
        int modifSpeed = gear.head.getStat().getSpeed() + gear.chest.getStat().getSpeed() +gear.bottom.getStat().getSpeed();
        int modifDodge = gear.head.getStat().getDodge() + gear.chest.getStat().getDodge() +gear.bottom.getStat().getDodge();
        int modifAccurency = gear.head.getStat().getAccuracy() + gear.chest.getStat().getAccuracy()  +gear.bottom.getStat().getAccuracy() ;
        int modifMaxHealth = gear.head.getStat().getMaxHealth() + gear.chest.getStat().getMaxHealth() +gear.bottom.getStat().getMaxHealth();
        this.stat = new Stat(basicStats.getAttack() + modifAttack, basicStats.getDefense() + modifDefense, basicStats.getSpeed() + modifSpeed, basicStats.getDodge() + modifDodge, basicStats.getAccuracy() + modifAccurency, basicStats.getMaxHealth() + modifMaxHealth);
    }

    // Reset the cooldown of the weapon
    private void resetWeapon()
    {
        this.gear.rightArm.resetWeapon();
        this.gear.leftArm.resetWeapon();
    }

    // Set the value of the smalest range between weapons
    private void initMinRange()
    {
        this.minRange = gear.leftArm.getRange();
        if(this.minRange > gear.rightArm.getRange())
        {
            this.minRange = gear.rightArm.getRange();
        }
    }

    // We check if the user respect the number max of stats in this robot
    private Stat verifBasicsStats(int attack, int defense, int speed, int dodge, int accuracy, int maxHealth)
    {
        Stat basicsStats = new Stat(attack,defense,speed,dodge,accuracy,maxHealth);
        //If the player try to cheat with the robot's stats, the stat are reset to 1
        basicsStats.verifyStats();
        return basicsStats;
    }

    // Is used when it's the turn to this robot to play
    public void doTurn(Robot enemyRobot, SimWar battle)
    {
        // Moving or attack
        skill.firstAction(this, enemyRobot);
        // Enemy robot still alive statement
        if ( enemyRobot.isAlive() )
        {
            //Use special or attack
            skill.secondAction(this, enemyRobot, battle, enemyDistance);
        }
        // Reset the cooldown of the weapon
        resetWeapon();
    }

    // This function chooses the best weapon to use at this time
    public Weapon choseWeapon()
    {
        //Verify the range
        if(this.getGear().leftArm.getRange() >= enemyDistance && this.getGear().rightArm.getRange() >= enemyDistance)
        {
            //Chose the weapon which deal more damage
            if(this.getGear().leftArm.getStat().getAttack() > this.getGear().rightArm.getStat().getAttack() && this.getGear().leftArm.getIsntUsed())
            {
                return this.getGear().leftArm;
            }
            else
            {
                return this.getGear().rightArm;
            }
        }
        else
        {
            // Choose the weapon who has the range
            if(this.getGear().leftArm.getRange() >= enemyDistance && this.getGear().leftArm.getIsntUsed())
            {
                return this.getGear().leftArm;
            }
            else
            {
                return this.getGear().rightArm;
            }

        }

    }

    // Decrease the heath of the robot when it take a hit
    public void takeDamage(int damage)
    {
        this.stat.setHealth(this.stat.getHealth() - damage);
    }

    //Return if the robot is alive
    public Boolean isAlive()
    {
        return this.stat.getHealth() > 0;
    }

    // Printing attributes
    public String toString()
    {
        return ("Name : " + name + " HP : " + this.getStat().getHealth() + "/" + this.getStat().getMaxHealth());
    }



}
