package com.lux.simwar.robot;

import com.lux.simwar.IHM;
import com.lux.simwar.SimWar;
import com.lux.simwar.gear.Weapon;
import com.lux.simwar.specialSkill.SpecialSkill;

import java.lang.reflect.InvocationTargetException;
import java.util.Random;

final public class Skill {
    //Attributes
    private SpecialSkill special;
    private final IHM ihm = IHM.getIhmInstance();


    //Constructor
    public Skill(String specialSkillName) {
        try
        {
            Class<?> className = Class.forName("com.lux.simwar.specialSkill." + specialSkillName);
            Object object = className.getDeclaredConstructor().newInstance();
            this.special = (SpecialSkill) object;
        }
        catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException skillConstructorException)
        {
            skillConstructorException.printStackTrace();
        }
    }

    //Methods

    // This function is used by a robot for attack another robot.
    // First we look for the accuracy, if the robot not failed to attack.
    // After, we looked if the target robot managed to dodge the hit
    public void attack(Robot myRobot, Robot targetRobot, Weapon weapon){
        this.ihm.printConsole("Robot " + myRobot.getName() + " try to attack " + targetRobot.getName() + ".");
        // Rolling probability to touch targeted robot according to self accuracy
        Random randomizer = new Random();
        int rand_accuracy = randomizer.nextInt(30);
        // Robot aim targeted robot statement
        if ( rand_accuracy <= myRobot.getStat().getAccuracy()+weapon.getStat().getAccuracy())
        {
            this.ihm.printConsole("Robot " + myRobot.getName() + " attack " + targetRobot.getName() + ".");
            // Rolling probability of targeted robot to dodge incoming attack
            int rand_dodge = randomizer.nextInt(30);
            // Targeted robot dodge incoming attack statement
            if ( rand_dodge <= targetRobot.getStat().getDodge() )
            {
                this.ihm.printConsole("Robot " + targetRobot.getName() + " dodge attack from " + myRobot.getName() + ".");
            }
            // Targeted robot doesn't dodge incoming attack statement
            else
            {
                int damageDeal;
                int damage = myRobot.getStat().getAttack() + weapon.getStat().getAttack();
                weapon.useWeapon();
                // Target enemy's defense is greater than self attack statement
                if ( damage <= targetRobot.getStat().getDefense() )
                {
                    damageDeal = 1;
                }
                // Self attack is greater than target enemy's defense statement
                else
                {
                    damageDeal = damage - targetRobot.getStat().getDefense();
                }
                // Removing health to targeted robot according to self attack and targeted robot's defense
                targetRobot.takeDamage(damageDeal);
                this.ihm.printConsole("Robot " + myRobot.getName() + " deal " + damageDeal + " to robot " + targetRobot.getName() + ".");
            }
        }
        // Robot doesn't aim targeted robot statement
        else
        {
            this.ihm.printConsole("Robot " + myRobot.getName() + " missed to aim " + targetRobot.getName());
        }
    }

    // This function is used when a robot used thi special ability
    public boolean useSpecial(Robot selfRobot, Robot targetedRobot, SimWar battle) // TODO use singleton SimWar
    {
        // Robot got Repair special skill statement
        switch (this.special.getName())
        {
            case "Repair":
                // Robot lost 3 HP or more statement
                if ((selfRobot.getStat().getMaxHealth() - selfRobot.getStat().getHealth()) >= 3) {
                    this.ihm.printConsole("Robot " + selfRobot.getName() + " is repairing himself.");
                    this.special.repair(selfRobot);
                }
                // Robot haven't lost 3 HP or more statement
                else {
                    this.ihm.printConsole("Robot " + selfRobot.getName() + " haven't lost enough HP to repairing himself.");
                    return false;
                }
                break;
            // Robot got Escape special skill statement
            case "Escape":
                this.ihm.printConsole("Robot " + selfRobot.getName() + " is escaping far away this enemy.");
                this.special.escape(targetedRobot, battle);
                break;
            // Robot got Hack special skill statement
            case "Hack":
                this.ihm.printConsole("Robot " + selfRobot.getName() + " is hacking robot " + targetedRobot.getName() + ".");
                this.special.hack(targetedRobot);
                break;
            // Robot got an unknown special skill statement
            default:
                this.ihm.printConsole("3RR0R ! Robot " + selfRobot.getName() + " got a unknown special skill.");
                return false;
        }
        // Applying cooldown
        selfRobot.setSpecialCooldownRemain(this.special.getCooldown());
        return true;
    }

    // First action in a round
    // The robot first tries to attack the other robot if it has range.
    // If it can't, the robot moves to the other robot
    public void firstAction(Robot myRobot, Robot enemyRobot){

        // We check if the robot has the range
        if(myRobot.getMinRange() >= myRobot.getEnemyDistance()){
            attack(myRobot, enemyRobot, myRobot.choseWeapon());
        }
        else
        {
            //The robot moves to the other robot
            this.ihm.printConsole("Robot " + myRobot.getName() + " walk");
            myRobot.setEnemyDistance(Math.max(myRobot.getEnemyDistance() - myRobot.getStat().getSpeed(), myRobot.getMinRange()));
            System.out.println("Distance between each robot : " + myRobot.getEnemyDistance());
        }
    }

    // Second action in a turn
    // First, the robot tries to use this special ability
    // If it can't, it tries to attack
    public void secondAction(Robot myRobot, Robot enemyRobot, SimWar battle, int enemyDistance )
    {
        // Special skill is under cooldown statement
        if (myRobot.getSpecialCooldownRemain() > 0)
        {
            myRobot.setSpecialCooldownRemain(myRobot.getSpecialCooldownRemain()-1);
        }
        this.ihm.printConsole("Robot " + myRobot.getName() + " have " + myRobot.getSpecialCooldownRemain() + " turn of cooldown before using special skill.");
        boolean specialSkillUsed = false;
        // Special skill can be used statement
        if ( myRobot.getSpecialCooldownRemain()  == 0 )
        {
            this.ihm.printConsole("Robot " + myRobot.getName() + " use a special skill.");
            specialSkillUsed = useSpecial(myRobot, enemyRobot, battle);
        }
        // Special skill cannot be used statement
        else
        {
            this.ihm.printConsole("Robot " + myRobot.getName() + " cannot use a special skill.");
        }
        // Robot doesn't use a special skill statement
        if ( !specialSkillUsed && myRobot.getMinRange() >= enemyDistance)
        {
            attack(myRobot, enemyRobot, myRobot.choseWeapon());
        }else
        {
            this.ihm.printConsole("Robot " + myRobot.getName() + " can t attack");
        }
    }

}
