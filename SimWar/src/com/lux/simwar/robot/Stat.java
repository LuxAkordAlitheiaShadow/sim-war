package com.lux.simwar.robot;

final public class Stat {
    //Attributes
    private int attack;
    private int defense;
    private int speed;
    private int dodge;
    private int accuracy;
    private int maxHealth;
    private int health;

    //Constructor
    public Stat(int attack, int defense, int speed, int dodge, int accuracy, int maxHealth) {
        this.attack = attack;
        this.defense = defense;
        this.speed = speed;
        this.dodge = dodge;
        this.accuracy = accuracy;
        this.maxHealth = maxHealth;
        this.health = maxHealth;

    }

    // Methods.

    // Getters.

    public int getSpeed()
    {
        return this.speed;
    }

    public int getHealth()
    {
        return this.health;
    }

    public int getMaxHealth()
    {
        return this.maxHealth;
    }

    public int getAttack()
    {
        return this.attack;
    }

    public int getDefense()
    {
        return this.defense;
    }

    public int getDodge()
    {
        return this.dodge;
    }

    public int getAccuracy()
    {
        return this.accuracy;
    }

    // Setters.

    public void setHealth(int newHealth)
    {
        this.health = newHealth;
    }

    public void verifyStats()
    {
        //If the player try to cheat with the robot's stats, the stat are reset to 1
        if(this.attack + this.defense + this.speed + this.dodge + this.accuracy + this.maxHealth > 20)
        {
            this.attack = 1;
            this.defense = 1;
            this.speed = 1;
            this.dodge = 1;
            this.accuracy = 1;
            this.maxHealth = 1;
            this.health = 1;
        }
    }

    @Override
    public String toString() {
        return "Stat{" +
                "attack=" + attack +
                ", defense=" + defense +
                ", speed=" + speed +
                ", dodge=" + dodge +
                ", accuracy=" + accuracy +
                ", maxHealth=" + maxHealth +
                ", health=" + health +
                '}';
    }
}
