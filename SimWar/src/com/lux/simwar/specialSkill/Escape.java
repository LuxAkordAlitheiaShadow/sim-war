package com.lux.simwar.specialSkill;

import com.lux.simwar.SimWar;
import com.lux.simwar.robot.Robot;

public final class Escape extends SpecialSkill
{
    // Constructor
    public Escape()
    {
        this.name = "Escape";
        this.cooldown = 4;
    }

    // Methods.

    // Incrementing enemy distance
    @Override
    public void escape(Robot targetRobot, SimWar battle)
    {
        int newEnemyDistance = targetRobot.getEnemyDistance() + 30;
        int areaSize = battle.getAreaSize();
        // Robot reach the max area size statement
        if ( newEnemyDistance > areaSize )
        {
            newEnemyDistance = areaSize;
        }
        targetRobot.setEnemyDistance(newEnemyDistance);
    }
}
