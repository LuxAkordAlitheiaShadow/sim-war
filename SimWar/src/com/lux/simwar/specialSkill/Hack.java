package com.lux.simwar.specialSkill;

import com.lux.simwar.robot.Robot;

public final class Hack extends SpecialSkill
{
    // Constructor.
    public Hack()
    {
        this.name = "Hack";
        this.cooldown = 5;
    }

    // Methods.

    // Stunning target robot
    @Override
    public void hack(Robot targetRobot)
    {
        int newStunRemain = targetRobot.getStunRemain() + 2;
        targetRobot.setStunRemain(newStunRemain);
    }
}
