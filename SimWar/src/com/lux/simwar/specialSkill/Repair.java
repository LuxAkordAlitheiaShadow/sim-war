package com.lux.simwar.specialSkill;

import com.lux.simwar.robot.Robot;

public final class Repair extends SpecialSkill
{
    // Constructor.
    public Repair()
    {
        this.name = "Repair";
        this.cooldown = 4;
    }

    // Methods.

    // Repairing robot
    @Override
    public void repair(Robot targetRobot)
    {
        int newHealth = targetRobot.getStat().getHealth() + 3;
        int maxHealth = targetRobot.getStat().getMaxHealth();
        // Reach max health statement
        if ( newHealth > maxHealth )
        {
            newHealth = maxHealth;
        }
        targetRobot.getStat().setHealth(newHealth);
    }
}
