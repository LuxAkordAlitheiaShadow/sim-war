package com.lux.simwar.specialSkill;

import com.lux.simwar.SimWar;
import com.lux.simwar.robot.Robot;

public abstract class SpecialSkill
{
    // Attributes.
    protected String name;
    protected int cooldown;

    // Getters.
    public String getName()
    {
        return this.name;
    }

    public int getCooldown()
    {
        return this.cooldown;
    }

    // Child's methods.
    public void repair(Robot targetRobot) {}
    public void escape(Robot targetRobot, SimWar battle) {}
    public void hack(Robot targetRobot) {}
}
